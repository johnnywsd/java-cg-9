




public class RayTracer extends MISApplet  {

	private static int WIDTH = 400;
	private static int HEIGHT = 400;
	private static int FOCAL = 10;

	//----- THESE TWO METHODS OVERRIDE METHODS IN THE BASE CLASS

	double[] v = new double[3];
	double[] w = new double[3];
	//	double[] w = {0,1,0};
	//		double[] s = { -0.2, 0.2, 0.0, 0.21 };	//x, y, z, r
	double[] s = { 200, 200, 200, 100 };	//x, y, z, r
	
	//	double[] s = { 00, 00, 00, 100 };	//x, y, z, r
	double[] v_s = new double[3];
	double[] nn = new double[3];
	double[] t = new double[2];
	
	double[] v2 = new double[3];
	double[] w2 = new double[3];
//	double[] s2 = { 100, 100, 100, 50 };	//x, y, z, r
	double[] s2 = { 200, 200, 200, 50 };	//x, y, z, r
	double[] v_s2 = new double[3];
	double[] nn2 = new double[3];
	double[] t2 = new double[2];

	boolean solveQuadraticEquation(double A, double B, double C, double[] t) {

		double discriminant = B * B - 4 * A * C;
		if (discriminant < 0)
			return false;

		double d = Math.sqrt(discriminant);
		t[0] = (-B - d) / (2 * A);
		t[1] = (-B + d) / (2 * A);
		return true;
	}
	void diff(double[] v, double[] s, double[] v_s){
		for(int i=0;i<v.length && i<s.length; i++){
			v_s[i] = v[i]-s[i];
		}
	}

	double tmpA = 0;
	double tmpB = 0;
	double tmpC = 0;
	boolean raytrace(double[] v, double[] w, double[] t) {

		diff(v, s, v_s);
		tmpA = 1.0;
		tmpB = 2 * dotProduct(w, v_s);
		tmpC = dotProduct(v_s, v_s) - s[3] * s[3];

		return solveQuadraticEquation(tmpA, tmpB, tmpC, t);

		//	      return false;
	}
	
	boolean raytrace2(double[] v, double[] w, double[] t) {

		diff(v, s2, v_s2);
		tmpA = 1.0;
		tmpB = 2 * dotProduct(w, v_s2);
		tmpC = dotProduct(v_s2, v_s2) - s2[3] * s2[3];

		return solveQuadraticEquation(tmpA, tmpB, tmpC, t);

		//	      return false;
	}
	
	double[] tmpPoint = new double[3];
	double[] tmpNomal = new double[3];
	double[] A = {0.2,0.0,0.0};
	double[] D = {0.8,0.0,0.0};
	double[] S = {1.0,1.0,1.0};
	double   p = 10;
	
	double[] A2 = {0.2,0.0,0.0};
	double[] D2 = {0.1,0.8,0.0};
	double[] S2 = {1.0,1.0,1.0};
	double   p2 = 10;
	
	private double[][][] lights={
			{ { 1.0, 1.0, 1.0}, {0.8, 0.8, 0.8} },
			{ {-1.0,-1.0,-1.0}, {0.8, 0.8, 0.8} },
	};
	
	Material material = new Material();
	Material material2 = new Material();
	
	public void initFrame(double time) { // INITIALIZE ONE ANIMATION FRAME

		//       REPLACE THIS CODE WITH YOUR OWN TIME-DEPENDENT COMPUTATIONS, IF ANY.

		t_time = 10 * time;
		material.setAmbientColor(A);
		material.setDiffuseColor(D);
		material.setSpecularColor(S);
		material.setSpecularPower(p);
		
		material2.setAmbientColor(A2);
		material2.setDiffuseColor(D2);
		material2.setSpecularColor(S2);
		material2.setSpecularPower(p2);


	}
	
	double[] tmpS=new double[4];
	boolean flag1=false;
	boolean flag2=false;
	void draw() {

			      s2[0] = 200+ 210 * Math.sin(t_time/8);
		//	      s[1] = 0.2 * Math.cos(2 * time);
			      s2[2] = 200+ 210 * Math.cos(t_time/8);

		setPoint(v,200,200,-300);

		for(int i=0;i< WIDTH;i++){
			for(int j=0; j<HEIGHT;j++){
				//				setPoint(v,i,j,0);
				setPoint(w,i-200,j-200,300);

				normalize(w,w);
				
				flag1 = raytrace(v, w, t);
				flag2 = raytrace2(v, w, t2);
				if(flag1 && flag2){
					if(t[0]<t2[0]){
						for(int k=0;k<3;k++){
							tmpPoint[k] = v[k] +t[0]*w[k];
							tmpNomal[k] = tmpPoint[k]-s[k];
						}
						normalize(tmpNomal,tmpNomal);
						this.mapNomalToRgb(tmpNomal,material, dirE , rgbTmp);
	
						setPixel(i, j, rgbTmp);
					}
					else{
						for(int k=0;k<3;k++){
							tmpPoint[k] = v[k] +t2[0]*w[k];
							tmpNomal[k] = tmpPoint[k]-s2[k];
						}
						normalize(tmpNomal,tmpNomal);
						this.mapNomalToRgb(tmpNomal,material2, dirE , rgbTmp);
	
						setPixel(i, j, rgbTmp);
					}
				}
				else if(flag1 && !flag2){
					for(int k=0;k<3;k++){
						tmpPoint[k] = v[k] +t[0]*w[k];
						tmpNomal[k] = tmpPoint[k]-s[k];
					}
					normalize(tmpNomal,tmpNomal);
					this.mapNomalToRgb(tmpNomal,material, dirE , rgbTmp);

					setPixel(i, j, rgbTmp);
				}
				else if(!flag1 && flag2){
					for(int k=0;k<3;k++){
						tmpPoint[k] = v[k] +t2[0]*w[k];
						tmpNomal[k] = tmpPoint[k]-s2[k];
					}
					normalize(tmpNomal,tmpNomal);
					this.mapNomalToRgb(tmpNomal,material2, dirE , rgbTmp);

					setPixel(i, j, rgbTmp);
				}			
//				if (raytrace(v, w, t) || raytrace2(v,w,t2)) {
//					if(t[0]>t2[0]){
//						t[0] = t2[0];
////						t[1] = t2[1];
//						tmpS=Arrays.copyOf(s2, s2.length);
//						
//					}else{
//						tmpS=Arrays.copyOf(s, s.length);
//					}
//					for(int k=0;k<3;k++){
//						tmpPoint[k] = v[k] +t[0]*w[k];
//						tmpNomal[k] = tmpPoint[k]-tmpS[k];
//					}
//					normalize(tmpNomal,tmpNomal);
//					this.mapNomalToRgb(tmpNomal,material, dirE , rgbTmp);
//
//					setPixel(i, j, rgbTmp);
//				}
			}
		}
		


	}

	void setPoint(double[] point,double x,double y,double z){
		point[0] = x;
		point[1] = y;
		point[2] = z;
	}


	double t_time = 0;
	



	public void setLights(double [][][] lights){
		this.lights = lights;
	}

	@Override
	public void init(){
		super.init();

	}

	

	int rgbBak[] = {0,0,0};
	double centerX = 200;
	double centerY = 200;
	double  cwidth= 10;
	double space = 30;
	double r=0;
	double R =0;
	public void setPixel(int x, int y, int rgb[]) { // SET ONE PIXEL'S COLOR
		if(x>=0 && x<W && y>=0 && y<H){
			pix[x+y*W] = pack(rgb[0],rgb[1],rgb[2]);
		}

	}




	private int[] rgb_WHITE = {255,255,255};

	

	@Override
	public void computeImage(double time){
		initFrame(time); 
		for(int i=0;i<W;i++){
			for(int j=0;j<H;j++)
				setPixel(i,j,rgb_WHITE);
			//			setPixel(i,j,rgb_BLACK);
		}
		draw();

	} 


	


	//	private double[] d= new double[2];

	private int[] rgbTmp = new int[3];



	private double[] dirE = {0,0,1};


	double tmp1 = 0;
	double tmp2 = 0;
	double[] Reflection = new double[3];
	double[] tmpRGB = new double[3];
	double[] Ldr;
	private void mapNomalToRgb(double[] N, Material material, double[] dirE, int[] rgb){
		for(int i=0;i<rgb.length;i++){
			tmpRGB[i] = 0;
		}

		double[] Argb = material.getAmbientColor();
		double[] Drgb = material.getDiffuseColor();
		double[] Srgb = material.getSpecularColor();
		double p = material.getSpecularPower();



		//light[1] = Lcolor, light[0] = Ldir
		for(double[][] light : this.lights){
			Ldr = light[0].clone();
			normalize(Ldr, Ldr);
			//			tmp1 = 2 * dotProduct(light[0], N);
			tmp1 = 2 * dotProduct(Ldr, N);
			for(int i=0;i<3;i++){
				//				Reflection[i] = tmp1*N[i] - light[0][i];
				Reflection[i] = tmp1*N[i] - Ldr[i];
			}
			//			tmp1 = Math.max(0, dotProduct(light[0],N) );
			tmp1 = Math.max(0, dotProduct(Ldr,N) );
			tmp2 = Math.pow(Math.max(0, dotProduct(Reflection, dirE)), p );

			for(int i=0;i<3;i++){
				tmpRGB[i] += light[1][i]*( Drgb[i] * tmp1 + Srgb[i] * tmp2);
			}

		}

		for(int i=0;i<3;i++){
			tmpRGB[i] += Argb[i];
		}
		gammaCorrection(tmpRGB,rgb,0.45);
	}

	private double dotProduct(double[] a, double[] b){
		double rtn = 0;
		for(int i=0;i<a.length && i< b.length;i++){
			rtn += a[i]* b[i];
		}
		return rtn;
	}

	double tmpPoweredSum = 0;
	private void normalize(double[] src, double[] dst){
		tmpPoweredSum = 0;
		for(int i=0;i<src.length;i++){
			tmpPoweredSum += src[i]*src[i];
		}
		tmpPoweredSum = Math.sqrt(tmpPoweredSum);
		for(int i=0;i<src.length && i< dst.length;i++){
			dst[i] = src[i] / tmpPoweredSum;
		}
	}
	private void gammaCorrection(double[] src, int[] dst, double arg){
		for(int i=0;i<3;i++){
			dst[i] = (int) (255* Math.pow(src[i], arg) );
		}
	}

}
